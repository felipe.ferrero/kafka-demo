# Kafka Demo

## Running Kafka

```sh
docker-compose up
```

Kafka will be listening to `localhost:29092`

## Running test code

All test code is run on jupyter-notebooks
